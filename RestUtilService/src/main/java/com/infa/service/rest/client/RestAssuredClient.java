package com.infa.service.rest.client;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import com.infa.service.rest.client.interfaces.Client;
import com.infa.service.rest.client.modal.Response;

public class RestAssuredClient implements Client {

	private String JSESSIONID;
	private HashMap<String, String> HEADERS;
	private io.restassured.response.Response response;

	public RestAssuredClient(HashMap<String, String> HEADERS, HashMap<String, String> COOKIES) {
		this.JSESSIONID = COOKIES.get("JSESSIONID");
		this.HEADERS = HEADERS;
	}

	public RestAssuredClient(String JSESSIONID) {
		this.JSESSIONID = JSESSIONID;
	}

	public RestAssuredClient() {
	}

	public Response get(String url) {
		response = given().when().get(url);
		Response responseModal = new Response();
		responseModal.setResponseCode(response.getStatusCode());
		responseModal.setResponseStatus(response.getStatusLine());
		responseModal.setResponseBody(response.getBody().asPrettyString());
		responseModal.setResponse(response);
		return responseModal;
	}

	public Response get(String url, Map<String, ?> queryParam) {
		response=given().queryParams(queryParam).when().get(url);
		Response responseModal = new Response();
		responseModal.setResponseCode(response.getStatusCode());
		responseModal.setResponseStatus(response.getStatusLine());
		responseModal.setResponseBody(response.getBody().asPrettyString());
		responseModal.setResponse(response);
		return responseModal;
	}

	public Response get(String url, Map<String, ?> queryParam, Map<String, ?> Headers) {
		response=given().headers(Headers).queryParams(queryParam).when().get(url);
		Response responseModal = new Response();
		responseModal.setResponseCode(response.getStatusCode());
		responseModal.setResponseStatus(response.getStatusLine());
		responseModal.setResponseBody(response.getBody().asPrettyString());
		responseModal.setResponse(response);
		return responseModal;
	}

	public Response post(String url) {
		response=given().when().post(url);
		Response responseModal = new Response();
		responseModal.setResponseCode(response.getStatusCode());
		responseModal.setResponseStatus(response.getStatusLine());
		responseModal.setResponseBody(response.getBody().asPrettyString());
		responseModal.setResponse(response);
		return responseModal;
	}

	public Response post(String url, String payload, Map<String, ?> Headers) {
		response=given().headers(Headers).body(payload).when().post(url);
		Response responseModal = new Response();
		responseModal.setResponseCode(response.getStatusCode());
		responseModal.setResponseStatus(response.getStatusLine());
		responseModal.setResponseBody(response.getBody().asPrettyString());
		responseModal.setResponse(response);
		return responseModal;
	}

	public Response post(String url, String payload, Map<String, ?> Headers, Map<String, ?> queryParam) {
		response=given().headers(Headers).queryParams(queryParam).body(payload).when().post(url);
		Response responseModal = new Response();
		responseModal.setResponseCode(response.getStatusCode());
		responseModal.setResponseStatus(response.getStatusLine());
		responseModal.setResponseBody(response.getBody().asPrettyString());
		responseModal.setResponse(response);
		return responseModal;
	}
	
	public Response post(String url, File FilePath, Map<String, ?> Headers, Map<String, ?> queryParam) {
		response=given().headers(Headers).queryParams(queryParam).body(FilePath).when().post(url);
		Response responseModal = new Response();
		responseModal.setResponseCode(response.getStatusCode());
		responseModal.setResponseStatus(response.getStatusLine());
		responseModal.setResponseBody(response.getBody().asPrettyString());
		responseModal.setResponse(response);
		return responseModal;
	}

	public Response put(String url, String payload, Map<String, ?> Headers, Map<String, ?> queryParam) {
		response=given().headers(Headers).queryParams(queryParam).body(payload).when().put(url);
		Response responseModal = new Response();
		responseModal.setResponseCode(response.getStatusCode());
		responseModal.setResponseStatus(response.getStatusLine());
		responseModal.setResponseBody(response.getBody().asPrettyString());
		responseModal.setResponse(response);
		return responseModal;
	}

	public Response delete(String url) {
		response=given().when().delete(url);
		Response responseModal = new Response();
		responseModal.setResponseCode(response.getStatusCode());
		responseModal.setResponseStatus(response.getStatusLine());
		responseModal.setResponseBody(response.getBody().asPrettyString());
		responseModal.setResponse(response);
		return responseModal;
	}

}
