package com.infa.service.rest.client.interfaces;

import java.util.Map;

import com.infa.service.rest.client.modal.Response;

public interface Client {
	
	public Response get(String url);
	
	public Response get(String url, Map<String, ?> queryParam);

	public Response get(String url, Map<String, ?> queryParam, Map<String, ?> Headers);
	
	public Response post(String url);
	
	public Response post(String url, String payload, Map<String, ?> Headers);

	public Response post(String url, String payload, Map<String, ?> Headers, Map<String, ?> queryParam);
	
	public Response put(String url, String payload, Map<String, ?> Headers, Map<String, ?> queryParam);
	
	public Response delete(String url);
}
