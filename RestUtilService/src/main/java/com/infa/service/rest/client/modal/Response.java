package com.infa.service.rest.client.modal;

public class Response {

	private static ThreadLocal<Integer> responseCode = new ThreadLocal<Integer>();
	private static ThreadLocal<String> responseStatus = new ThreadLocal<String>();
	private static ThreadLocal<String> responseBody = new ThreadLocal<String>();
	private static ThreadLocal<Object> response = new ThreadLocal<Object>();

	public int getResponseCode() {
		return responseCode.get();
	}

	public void setResponseCode(int responseCode) {
		Response.responseCode.set(responseCode);
	}

	public String getResponseStatus() {
		return responseStatus.get();
	}

	public void setResponseStatus(String responseStatus) {
		Response.responseStatus.set(responseStatus);
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		Response.response.set(response);
	}

	public String getResponseBody() {
		return responseBody.get();
	}

	public void setResponseBody(String responseBody) {
		Response.responseBody.set(responseBody);
	}

}
